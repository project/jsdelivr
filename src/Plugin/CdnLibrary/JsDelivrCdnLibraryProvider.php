<?php

namespace Drupal\jsdelivr\Plugin\CdnLibrary;

use Drupal\cdn_library\Annotation\CdnLibraryUrl;
use Drupal\cdn_library\CdnLibraryResponse;
use Drupal\cdn_library\CdnLibraryResult;
use Drupal\cdn_library\Plugin\CdnLibraryProviderBase;
use Drupal\jsdelivr\JsDelivr;
use Drupal\plus\Utility\Attachments;

/**
 * @CdnLibraryProvider(
 *   id = "jsdelivr",
 *   label = @Translation("jsDelivr"),
 *   version = 1,
 *   urls = {
 *     @CdnLibraryUrl("asset",    url = "https://cdn.jsdelivr.net/{name}@{version}/{file}"),
 *     @CdnLibraryUrl("files",    url = "https://data.jsdelivr.com/v1/package/{name}@{version}/flat"),
 *     @CdnLibraryUrl("info",     url = "https://api.npms.io/v2/package/{name}"),
 *     @CdnLibraryUrl("versions", url = "https://data.jsdelivr.com/v1/package/{name}"),
 *     @CdnLibraryUrl("search",   url = "https://api.npms.io/v2/search?q={query}"),
 *   },
 * )
 */
class JsDelivrCdnLibraryProvider extends CdnLibraryProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getIdentifierSeparator() {
    return '@';
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeLibraryName($name, CdnLibraryUrl $url = NULL) {
    $name = parent::normalizeLibraryName($name);

    // Prepends the library name with the relevant GitHub or NPM prefix based
    // on whether the library name contains a forward slash to indicate
    // user/repo (GitHub) vs. a single package (NPM).
    if ($url && preg_match('`^https:\/\/(cdn|data)\.jsdelivr\.`', $url->getUrl(TRUE))) {
      preg_match('/^(gh|npm)\//', $name, $matches);
      $type = !empty($matches[1]) ? $matches[1] : 'npm';
      $name = preg_replace('/^(gh|npm)\//', '', $name);
      return "$type/$name";
    }
    // Removes any gh/npm prefix.
    else {
      $name = preg_replace('/^(gh|npm)\//', '', $name);
    }

    return $name;
  }

  /**
   * @param array $files
   * @param \Drupal\cdn_library\CdnLibraryResponse $response
   *
   * @return \Drupal\plus\Utility\AttachmentsInterface
   *   An Attachments object.
   */
  protected function parseFiles(array $files, CdnLibraryResponse $response) {
    $attachments = Attachments::create();
    foreach ($files as $file) {
      if (isset($file['name']) && preg_match('/(\.min)?\.(css|js)$/', $file['name'], $matches)) {
        $type = $matches[2];
        $data = [];

        // Prefer minified assets.
        if (!empty($matches[1])) {
          $data['minified'] = TRUE;
        }

        // Then prefer distributed assets.
        if (preg_match('/\/(dist|lib|src)/', $file['name'])) {
          $data['distributed'] = TRUE;
        }

        $attachments->attach($type, [$file['name'] => $data]);
      }
    }
    $this->alter('jsdelivr_parse_files', $attachments, $files, $response);
    return $attachments;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseResponse(CdnLibraryResponse $response) {
    // Immediately return an empty array if response was invalid.
    if ($response->getStatusCode() !== 200) {
      return [];
    }

    $url = $response->getUrl();
    $identifier = $url->getIdentifier();
    $json = $response->getJson();
    switch ($url->getType()) {
      // Info.
      case CdnLibraryUrl::FILES:
        $assets = [];
        if ($json['files']) {
          $assets = $this->parseFiles($json['files'], $response);
        }
        $library = JsDelivr::create($identifier, $assets);
        return $library;

      // Search.
      case CdnLibraryUrl::SEARCH:
        $results = [];
        foreach ($json['results'] as $data) {
          $name = $data['package']['name'];
          $results[$name] = CdnLibraryResult::create($name, $this, $data);
        }
        return $results;

      // Versions.
      case CdnLibraryUrl::VERSIONS:
        return $json['versions'];

      default:
        return $json;
    }
  }

}
